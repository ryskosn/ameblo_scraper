package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func getAmebaID(u *url.URL) string {
	// https://ameblo.jp/ebizo-ichikawa/entry-12382278427.html
	return strings.Split(u.Path, "/")[1]
}

func getBlogID(doc *goquery.Document) string {
	href, _ := doc.Find("head > link").Eq(32).Attr("href")

	// header_10039162931.css
	f := path.Base(href)
	f = strings.Replace(f, "header_", "", -1)
	id := strings.Replace(f, path.Ext(f), "", -1)
	return id
}

func getEntryID(u *url.URL) string {
	// entry-12381664603.html
	f := path.Base(u.Path)
	f = strings.Replace(f, "entry-", "", -1)
	id := strings.Replace(f, path.Ext(f), "", -1)
	return id
}

func trimText(s string) string {
	s = strings.Trim(s, "<br />")
	for {
		if strings.Contains(s, "<br /><br /><br />") {
			s = strings.Replace(s, "<br /><br /><br />", "<br /><br />", -1)
			fmt.Println("replace br")
		} else {
			break
		}
	}
	s = strings.Replace(s, `"`, `'`, -1)
	return s
}

func exists(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil
}

func downloadFile(u *url.URL, p string) error {
	resp, err := http.Get(u.String())
	if err != nil {
		log.Printf("Failed to download: %s", path.Base(u.String()))
		return err
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Failed to read file: %s", path.Base(u.String()))
		return err
	}

	if err := ioutil.WriteFile(p, b, 0666); err != nil {
		log.Printf("Failed to write file: %s", p)
		return err
	}
	return nil
}
