package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	dproxy "github.com/koron/go-dproxy"
)

const UserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.17 Safari/537.36"

func constructCommentsReqURL(limit, offset int) string {
	baseCommentsURL := "https://ameblo.jp/_api/blogComments;amebaId=%s;blogId=%s;entryId=%s;limit=%d;offset=%d?returnMeta=true"
	return fmt.Sprintf(baseCommentsURL, e.amebaID, e.blogID, e.entryID, limit, offset)
}

func fetchCommentsJSON(limit, offset int) interface{} {
	reqURL := constructCommentsReqURL(limit, offset)

	client := &http.Client{}
	req, err := http.NewRequest("GET", reqURL, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("User-Agent", UserAgent)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Comments Request error:", err)
	}
	log.Println("Comments Response:", resp.StatusCode)
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatal(resp)
	}

	decoder := json.NewDecoder(resp.Body)
	var result interface{}
	decoder.Decode(&result)
	return result
}

func getCommentsCount(v interface{}) (int, error) {
	p := dproxy.New(v).M("data")
	total, err := p.M("paging").M("total_count").Int64()
	if err != nil {
		log.Println("Faild to get total_count:", err)
		return 0, err
	}
	return int(total), nil
}

func decodeCommentsJSON(v interface{}, count int) []comment {
	p := dproxy.New(v).M("data")
	ids, err := p.M("commentIds").Array()
	if err != nil {
		log.Fatalf("Faild to get array of commentIds: %v", err)
	}
	var cs []comment
	for i, id := range ids {
		v, _ := id.(float64)
		log.Println("id:", v)
		idStr := strconv.Itoa(int(v))
		cmap, _ := p.M("commentMap").M(idStr).Map()
		c := comment{
			Num:    strconv.Itoa(count - i),
			Title:  cmap["comment_title"].(string),
			Text:   cmap["comment_text"].(string),
			Author: cmap["comment_name"].(string),
		}
		c.Title = trimText(c.Title)
		c.Text = trimText(c.Text)
		c.Author = trimText(c.Author)
		cs = append(cs, c)
	}
	return cs
}

func getComments(limit, offset, count int) []comment {
	v := fetchCommentsJSON(limit, offset)
	return decodeCommentsJSON(v, count)
}

func setComments() {
	v := fetchCommentsJSON(50, 0)
	e.CCount, err = getCommentsCount(v)
	if err != nil {
		log.Println("Failed to get comment count:", err)
		return
	}

	cs := decodeCommentsJSON(v, e.CCount)
	switch {
	case 0 < e.CCount && e.CCount <= 50:
		e.Comments = cs
	case e.CCount > 50:
		pages := e.CCount / 50
		rest := e.CCount % 50
		for i := 1; i < pages; i++ {
			comments := getComments(50, 50*i, e.CCount-50*i)
			cs = append(cs, comments...)
			time.Sleep(time.Second * 1)
		}
		if rest != 0 {
			comments := getComments(rest, 50*pages, e.CCount-50*pages)
			cs = append(cs, comments...)
		}
		e.Comments = cs
	}
}
