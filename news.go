package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"path"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	dproxy "github.com/koron/go-dproxy"
)

const newsURL = "https://news.ameba.jp/search/?%s"

func getName() string {
	name := doc.Find("p.skin-profileName > a").Text()

	// old version
	if name == "" {
		name = doc.Find("div.profileUserNickname > em > a").Text()
	}
	return strings.Replace(strings.TrimSpace(name), " ", "", -1)
}

// 使わない
func parseNewsPage(doc *goquery.Document) []article {
	articles := doc.Find("ul.SearchResultModule__list--14pA0 > li")
	articles = articles.FilterFunction(func(_ int, s *goquery.Selection) bool {
		return s.Find("div > p").Eq(1).Text() == "アメーバニュース/ブログ発"
	})

	// url を補完する
	replaceHref := func(_ int, s *goquery.Selection) {
		baseURL := "https://news.ameba.jp/"
		a := s.Find("div > h2").Contents()
		p, _ := a.Attr("href")
		fullURL := path.Join(baseURL, p)
		a.SetAttr("href", fullURL)
		a.SetAttr("target", "_blank")
	}
	articles.Each(replaceHref)

	var ns []article
	articles.Each(func(_ int, s *goquery.Selection) {
		var n article
		a, _ := s.Find("div > h2").Html()
		n.Title = a
		n.Date = s.Find("div > p").Eq(0).Text()
		ns = append(ns, n)
	})
	return ns
}

func encodeNewsQuery(name string) string {
	v := url.Values{}
	v.Add("q", name+" ブログ")
	query := strings.Replace(v.Encode(), "q=", "", -1)
	return strings.Replace(query, `+`, `%20`, -1)
}

func constructNewsReqURL(limit, page int, query string) string {
	u := "https://api-news.ameba.jp/v1.0/entries/search?limit=%d&page=%d&q=%s"
	return fmt.Sprintf(u, limit, page, query)
}

func fetchNewsJSON(limit, page int, query string) interface{} {
	reqURL := constructNewsReqURL(limit, page, query)

	client := &http.Client{}
	req, err := http.NewRequest("GET", reqURL, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("User-Agent", UserAgent)
	req.Header.Set("Access-Control-Request-Headers", "x-news-api-request-id")
	req.Header.Set("X-News-API-Request-Id", "AmebaNews")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("News Request error:", err)
	}
	log.Println("News Response:", resp.StatusCode)
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatal(resp)
	}

	decoder := json.NewDecoder(resp.Body)
	var result interface{}
	decoder.Decode(&result)

	// b, _ := ioutil.ReadAll(resp.Body)
	// result := string(b)
	return result
}

func getNewsCount(v interface{}) int {
	total, err := dproxy.New(v).M("total").Int64()
	if err != nil {
		log.Fatalf("Faild to get(decode) total news count: %v", err)
	}
	return int(total)
}

func decodeNewsJSON(v interface{}, name string) []article {
	total := getNewsCount(v)
	fmt.Println("news count total:", total)
	as, err := dproxy.New(v).M("data").Array()
	if err != nil {
		log.Fatalf("Faild to get(decode) array of articles: %v", err)
	}

	var ns []article
	for _, a := range as {
		d := dproxy.New(a)
		media, _ := d.M("media").M("description").String()
		title, _ := d.M("title").String()

		if media == "アメーバニュース/ブログ発" && strings.Contains(title, name) {
			// if media == "アメーバニュース/ブログ発" {
			title = strings.Replace(title, `<span class="hlword1">`, "", -1)
			title = strings.Replace(title, `</span>`, "", -1)

			url, _ := d.M("url").String()

			dateStr, _ := d.M("public_datetime").String()
			s := strings.Replace(dateStr, ".000Z", "", -1)
			format := "2006-01-02T15:04:05"
			t, _ := time.Parse(format, s)

			n := article{
				Title:  title,
				URLStr: url,
				Date:   t.Format("2006/01/02 15:04"),
			}
			ns = append(ns, n)
		}
	}
	return ns
}

func getNews(limit, page int, query, name string) []article {
	v := fetchNewsJSON(limit, page, query)
	return decodeNewsJSON(v, name)
}

func setNews() {
	name := getName()
	q := encodeNewsQuery(name)
	v := fetchNewsJSON(20, 1, q)

	count := getNewsCount(v)
	ns := decodeNewsJSON(v, name)

	switch {
	case 0 <= count && count <= 20:
		e.News = ns
	case count > 20:
		pages := count / 20
		rest := count % 20
		for i := 1; i < pages; i++ {
			news := getNews(20, i+1, q, name)
			ns = append(ns, news...)
			time.Sleep(time.Millisecond * 900)
		}
		if rest != 0 {
			news := getNews(rest, pages+1, q, name)
			ns = append(ns, news...)
		}
		e.News = ns
	}
	e.NCount = len(ns)
}
