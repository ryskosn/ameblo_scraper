package main

import (
	"log"
	"strings"
)

func setTitle() {
	t, err := doc.Find("title").Html()
	if err != nil {
		log.Fatalf("Failed to find title: %v", err)
	}
	title := strings.Split(t, "|")[0]
	title = strings.Trim(title, " ")
	e.Title = title
}
