package main

import (
	"io/ioutil"
	"log"
	"os"
	"text/template"
)

const (
	entryFile    = "entry.html"
	entryTmpl    = "/templates/layout.html"
	commentsTmpl = "/templates/layout_comments.html"
	newsTmpl     = "/templates/layout_news.html"
)

func parseAssets(path string) *template.Template {
	f, err := Assets.Open(path)
	if err != nil {
		log.Fatalf("Failed to open assets: %v", err)
	}
	defer f.Close()
	b, err := ioutil.ReadAll(f)
	t := template.New("t")
	return template.Must(t.Parse(string(b)))
}

func (e *entry) writeEntryHTML() {
	tmpl := parseAssets(entryTmpl)
	file, err := os.Create(entryFile)
	if err != nil {
		log.Fatal(err)
	}
	if err := tmpl.Execute(file, e); err != nil {
		log.Fatalf("Failed to render entry template: %v", err)
	}
}

func (e *entry) writeCommentsHTML() {
	tmpl := parseAssets(commentsTmpl)
	file, err := os.Create("comments.html")
	if err != nil {
		log.Fatal(err)
	}
	if err := tmpl.Execute(file, e); err != nil {
		log.Fatalf("Failed to render comments template: %v", err)
	}
}

func (e *entry) writeNewsHTML() {
	tmpl := parseAssets(newsTmpl)
	file, err := os.Create("news.html")
	if err != nil {
		log.Fatal(err)
	}
	if err := tmpl.Execute(file, e); err != nil {
		log.Fatalf("Failed to render news template: %v", err)
	}
}
