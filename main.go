package main

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/atotto/clipboard"
)

type entry struct {
	url      *url.URL
	URLStr   string
	amebaID  string
	blogID   string
	entryID  string
	Title    string
	Body     []string
	CCount   int
	Comments []comment
	NCount   int
	News     []article
}

type comment struct {
	Num    string `json:"num"`
	Title  string `json:"title"`
	Text   string `json:"text"`
	Author string `json:"author"`
}

type article struct {
	Title  string
	URLStr string
	Date   string
}

var (
	doc *goquery.Document
	e   entry
	err error
)

func init() {
	// rawurl := "https://ameblo.jp/ebizo-ichikawa/entry-12382278427.html"
	// rawurl := "https://ameblo.jp/alexander1203/entry-12382908324.html"
	rawurl, _ := clipboard.ReadAll()

	e.url, err = url.Parse(rawurl)
	if err != nil {
		log.Fatal(err)
	}
	e.URLStr = e.url.String()

	doc, err = goquery.NewDocument(e.URLStr)
	if err != nil {
		log.Fatal(err)
	}
	e.amebaID = getAmebaID(e.url)
	e.blogID = getBlogID(doc)
	e.entryID = getEntryID(e.url)
}

func init() {
	// check static files
	const staticDir = "static"

	const incsearchjs = "https://raw.githubusercontent.com/onozaty/incsearch.js/master/src/incsearch.js"
	incURL, err := url.Parse(incsearchjs)
	if err != nil {
		log.Fatalf("Failed to parse url: %v", err)
	}
	incpath := filepath.Join(staticDir, "incsearch.js")

	const skeltoncss = "https://raw.githubusercontent.com/dhg/Skeleton/master/css/skeleton.css"
	skelURL, err := url.Parse(skeltoncss)
	if err != nil {
		log.Fatalf("Failed to parse url: %v", err)
	}
	skelpath := filepath.Join(staticDir, "skelton.css")

	if !exists(staticDir) {
		fmt.Println("there is not static/")
		if err := os.Mkdir(staticDir, 0777); err != nil {
			log.Println(err)
		}
	}
	if !exists(incpath) {
		fmt.Println("downloading incsearch.js ...")
		if err := downloadFile(incURL, filepath.Join(staticDir, "incsearch.js")); err != nil {
			log.Println(err)
		}
	}
	if !exists(skelpath) {
		fmt.Println("downloading skelton.css ...")
		if err := downloadFile(skelURL, filepath.Join(staticDir, "skelton.css")); err != nil {
			log.Println(err)
		}
	}
}

func main() {
	setTitle()
	setEntryBody()
	setComments()
	setNews()

	e.writeEntryHTML()
	e.writeCommentsHTML()
	e.writeNewsHTML()

	fmt.Println("entry.html をブラウザで開きます")
	time.Sleep(time.Second * 1)

	var c *exec.Cmd
	switch runtime.GOOS {
	case "darwin":
		c = exec.Command("open", entryFile)
	case "windows":
		c = exec.Command("cmd", "/C", "start", entryFile)
	}
	if err := c.Start(); err != nil {
		log.Println("Failed to open entry file:", err)
	}
}
