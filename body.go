package main

import (
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func setEntryBody() {
	body := doc.Find("#entryBody").Find("div")
	br := body.Has("br")
	body = body.NotSelection(br)
	lines := body.Map(func(_ int, s *goquery.Selection) string {
		div, _ := s.Html()
		return strings.Trim(div, " 　\n\t")
	})

	// 画像リンク以外の文章部分は一つにまとめる
	var result, tmp []string
	for _, line := range lines {
		if strings.HasPrefix(line, "<a") {
			if len(tmp) > 0 {
				result = append(result, strings.Join(tmp, "<br>"))
				tmp = []string{}
			}
			result = append(result, line)
		} else {
			tmp = append(tmp, line)
			continue
		}
	}

	// tmp に残ったものがあれば result に追加する
	if len(tmp) > 0 {
		result = append(result, strings.Join(tmp, "<br>"))
	}
	e.Body = result
}
